FROM debian:latest AS build

RUN apt-get update && apt-get install -y dh-autoreconf

RUN mkdir build
COPY . /build
WORKDIR /build

RUN autoreconf -fis
RUN ./configure
RUN make all || true

FROM alpine:latest

COPY --from=build /build/src/z80asm /bin/z80asm
COPY --from=build /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libc.so.6
COPY --from=build /lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2
RUN mkdir workspace

WORKDIR /workspace
ENTRYPOINT [ "/bin/z80asm" ]
